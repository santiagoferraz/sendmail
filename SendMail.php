<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class SendMail
{
    private $mail;
    private $dotenv;

    public function __construct()
    {
        // PHPMAILER INSTANCE
        $this->mail = new PHPMailer(true);

        // TO DEBUG LOG MAIL
        // $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;

        $this->mail->isSMTP();
        $this->mail->Host       = getenv('SMTP_HOST');
        $this->mail->Username   = getenv('EMAIL');
        $this->mail->Password   = getenv('PASSWORD');
        $this->mail->Port       = getenv('SMTP_PORT');
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $this->mail->SMTPAuth   = true;
        $this->mail->isHTML(true);
        $this->mail->CharSet = 'UTF-8';
        $this->mail->setFrom(getenv('FROM_EMAIL'), getenv('FROM_NAME'));
    }

    public function send($email, $name, $subject, $body) {
        $this->mail->ClearAllRecipients();
        $this->mail->addAddress($email, $name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $body;

        try {
            $this->mail->send();
            return 'E-mail enviado com sucesso para: '.$email;
        } catch (Exception $e) {
            return 'Erro ao enviar e-mail para: '. $email;
        }
    }
}