<?php
require 'vendor/autoload.php';

class UsuariosModel
{
    private $mysqli;
    public function __construct()
    {
        $this->mysqli = new mysqli(getenv('DB_HOST').':'.getenv('DB_PORT')
                                        ,getenv('DB_USER')
                                        ,getenv('DB_PASSWORD')
                                        ,getenv('DB_DATABASE'));

        if ($this->mysqli->connect_errno) {
            echo "Falha ao tentar conectar Banco de Dados: " . $this->mysqli -> connect_error;
            exit();
        }
    }

    public function getUsuariosNotifica() {
        $sql = "(SELECT
                    u.id
                    , u.inicio
                    , u.data_fim
                    , u.nome
                    , u.email
                    , e.nome as empresa
                    , e.site
                    , e.telefone
                    , DATEDIFF(u.data_fim, now()) as qtde_dias_restante
                FROM usuarios u
                INNER JOIN empresas e ON (u.empresa_id = e.id)
                WHERE
                    u.status_id = 1
                    AND u.empresa_id IN (1, 2)
                    AND u.email != '' 
                    AND DATEDIFF(u.data_fim, now()) <= ".getenv('QTDE_DIAS_NOTIFICAR')."
        )";

        $retorno = array();
        if ($result = $this->mysqli->query($sql)) {
            while($row = mysqli_fetch_assoc($result)) {
                array_push($retorno, $row);
            }
        }
        $this->mysqli->close();
        return $retorno;
    }

    public function setBlockUser() {
        $sql = "UPDATE usuarios u
                SET u.status_id = 2
                WHERE
                    u.status_id = 1
                    AND u.empresa_id IN (1, 2)
                    AND DATEDIFF(u.data_fim, now()) <= 0
        ";
        $result = $this->mysqli->query($sql);
        return $result;
    }
}