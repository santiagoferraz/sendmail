<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send Mail</title>
</head>
<body style="border: 1px solid #ccc; background-color: #fff; border-radius: 10px; padding: 10px;">
<div class="header" style="max-height: 90px; border: 3px solid #e5e5e5; border-radius: 10px; background: #f1f3f4; text-align: center;">
    <img style="height: 90px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Service_mark.svg/1280px-Service_mark.svg.png" alt="logo">
</div>
<div class="content" style="padding: 10px 25px; font-size: 12pt;">
    @yield('content')
</div>
<div class="footer" style="max-height: 90px; border: 3px solid #e5e5e5; border-radius: 10px; background: #f1f3f4; text-align: center;">
    <p>Email enviado automáticamente, favor não responder.<br>
        Em caso de dúvidas entre em contato conosco através da página de contato no site.<br>
        Para garantir o recebimento dos nossos emails, favor adicione <b>sistema@dominio.com.br</b> na sua lista de contatos.</p>
</div>
</body>
</html>