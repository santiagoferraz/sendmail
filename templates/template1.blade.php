@extends('layouts.header-footer1')

@section('content')
    <h2>Olá {{ utf8_encode($variables['nome']) }}, </h2>
    <p>Tudo bem com você?</p>
    <p>
        O prazo de uso da sua conta se encerra em <b>{{ date("d/m/Y", strtotime($variables['data_fim'])) }}</b>.
        Para continuar usando nossos serviços você deve migrar de plano em
        <a href="www.link.com.br"><b>{{ getenv('LINK_PARA_ASSINAR') }}</b></a>
        <br>
    <p>Não esqueça, você tem apenas <b>{{ $variables['qtde_dias_restante'] }} dias</b>.</p>
    </p>
@endsection