<?php
use eftec\bladeone\BladeOne;
require __DIR__.'/../SendMail.php';
require __DIR__.'/../models/UsuariosModel.php';

class StartController
{
    protected $views;
    protected $cache;
    protected $blade;
    public function __construct()
    {
        $this->views = __DIR__ . '/../templates';
        $this->cache = __DIR__ . '/../cache';
        $this->blade = new BladeOne($this->views, $this->cache, BladeOne::MODE_AUTO);
    }

    public function startSendMail()
    {
        // BLOQUEIA ACESSO DOS USUÁRIOS QUE NÃO RENOVARAM
        (new UsuariosModel())->setBlockUser();

        $usuariosNotificar = (new UsuariosModel())->getUsuariosNotifica();

        $sendmail = new SendMail();
        $logAll = array();
        foreach ($usuariosNotificar as $usuario) {
            $log = $sendmail->send($usuario['email']
                                            , $usuario['nome']
                                            , 'Testando email'
                                            , $this->blade->run("template1", $usuario));
            array_push($logAll, $log);
        }

        foreach (explode(',', getenv('EMAIL_ADM')) as $emailAdm) {
            $sendmail->send($emailAdm
                            , 'ADM do sistema'
                            , 'Log de emails enviados'
                            , $this->blade->run("template2", $logAll));
        }

    }
}