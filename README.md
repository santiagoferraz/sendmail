# Sendmail

Projeto para envio de noptificação

# Download

Faça o clone do projeto com o comando:
**`git clone https://gitlab.com/santiagoferraz/sendmail`**

Faça a instalação das dependências com o comando:
**`composer install`**

# Configurações

Após realizar o clone do project, renomeie o arquivo "**.env.example**" para "**.env**" e em seguida configure com os dados do ambiente.

# Adicionar na CRON

Para adicionar na CRON, basta inserir o comando abaixo. Modifique conforme o ambiente.

`* * * * * root cd /home/san/Documentos/jef/sendmail && php index.php"`

